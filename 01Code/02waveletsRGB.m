% Slike za rad:
% SlikeZaCode/dodik.jpg
% football.tif
% cameraman.tif
%

% ----------------------
% Doubuche
% ----------------------

clear all ; close all

x = imread('football.jpg') ;
figure; imshow(x);

%Average of red componet, Horizontal details of red component, Vertical details of red component, Diagonal details of red component
[xar,xhr,xvr,xdr] = dwt2(x(:,:,1),'db2'); %Red component - Doubuche mother wavelet
%Green
[xag,xhg,xvg,xdg] = dwt2(x(:,:,2),'db2'); %Green component - Doubuche mother wavelet
%Blue
[xab,xhb,xvb,xdb] = dwt2(x(:,:,3),'db2'); %Blue component - Doubuche mother wavelet

%Average image - red, green, blue
xa(:,:,1) = xar ; xa(:,:,2) = xag ; xa(:,:,3) = xab ; %
%Horizontal details - red, green, blue
xh(:,:,1) = xhr ; xh(:,:,2) = xhg ; xh(:,:,3) = xhb ;
%Vertical details - red, green, blue
xv(:,:,1) = xvr ; xv(:,:,2) = xvg ; xv(:,:,3) = xvb ;
%Diagonal details - red, green, blue
xd(:,:,1) = xdr ; xd(:,:,2) = xdg ; xd(:,:,3) = xdb ;

figure, imshow(xa/255); %xa je tipa double pa moramo podijeliti sa 255
figure, imshow(xh);
figure, imshow(xv);
figure, imshow(xd);
X1 = [ xa*0.003 log10(xv)*0.3 ; log10(xh)*0.3 log10(xd)*0.3 ];
figure ; imshow(X1)

%Dalje - level 2
[xaar,xhhr,xvvr,xddr] = dwt2(xa(:,:,1),'haar');
[xaag,xhhg,xvvg,xddg] = dwt2(xa(:,:,2),'haar');
[xaab,xhhb,xvvb,xddb] = dwt2(xa(:,:,3),'haar');

xaa(:,:,1) = xaar ; xaa(:,:,2) = xaag ; xaa(:,:,3) = xaab ;
xhh(:,:,1) = xhhr ; xhh(:,:,2) = xhhg ; xhh(:,:,3) = xhhb ;
xvv(:,:,1) = xvvr ; xvv(:,:,2) = xvvg ; xvv(:,:,3) = xvvb ;
xdd(:,:,1) = xddr ; xdd(:,:,2) = xddg ; xdd(:,:,3) = xddb ;

figure, imshow(xaa/255); %xa je tipa double pa moramo podijeliti sa 255
figure, imshow(xhh);
figure, imshow(xvv);
figure, imshow(xdd);
X11 = [ xaa*0.001 log10(xvv)*0.3 ; log10(xhh)*0.3 log10(xdd)*0.3 ] ;
figure ; imshow(X11)

[r,c,s] = size(xv) ;
figure ; imshow([X11(1:r,1:c,:) xv*0.05 ; xh*0.05 xd*0.05 ])

% ----------------------
% haar
% ----------------------

clear all ; close all
x = imread('football.jpg') ;
figure; imshow(x);

%Average of red componet, Horizontal details of red component, Vertical details of red component, Diagonal details of red component
[xar,xhr,xvr,xdr] = dwt2(x(:,:,1),'haar'); %Red component - Doubuche mother wavelet
%Green
[xag,xhg,xvg,xdg] = dwt2(x(:,:,2),'haar'); %Green component - Doubuche mother wavelet
%Blue
[xab,xhb,xvb,xdb] = dwt2(x(:,:,3),'haar'); %Blue component - Doubuche mother wavelet

%Average image - red, green, blue
xa(:,:,1) = xar ; xa(:,:,2) = xag ; xa(:,:,3) = xab ; %
%Horizontal details - red, green, blue
xh(:,:,1) = xhr ; xh(:,:,2) = xhg ; xh(:,:,3) = xhb ;
%Vertical details - red, green, blue
xv(:,:,1) = xvr ; xv(:,:,2) = xvg ; xv(:,:,3) = xvb ;
%Diagonal details - red, green, blue
xd(:,:,1) = xdr ; xd(:,:,2) = xdg ; xd(:,:,3) = xdb ;

figure, imshow(xa/255); %xa je tipa double pa moramo podijeliti sa 255
figure, imshow(xh);
figure, imshow(xv);
figure, imshow(xd);
X1 = [ xa*0.003 log10(xv)*0.3 ; log10(xh)*0.3 log10(xd)*0.3 ];
figure ; imshow(X1)

%Dalje - level 2
[xaar,xhhr,xvvr,xddr] = dwt2(xa(:,:,1),'db2');
[xaag,xhhg,xvvg,xddg] = dwt2(xa(:,:,2),'db2');
[xaab,xhhb,xvvb,xddb] = dwt2(xa(:,:,3),'db2');

xaa(:,:,1) = xaar ; xaa(:,:,2) = xaag ; xaa(:,:,3) = xaab ;
xhh(:,:,1) = xhhr ; xhh(:,:,2) = xhhg ; xhh(:,:,3) = xhhb ;
xvv(:,:,1) = xvvr ; xvv(:,:,2) = xvvg ; xvv(:,:,3) = xvvb ;
xdd(:,:,1) = xddr ; xdd(:,:,2) = xddg ; xdd(:,:,3) = xddb ;

figure, imshow(xaa/255); %xa je tipa double pa moramo podijeliti sa 255
figure, imshow(xhh);
figure, imshow(xvv);
figure, imshow(xdd);
X11 = [ xaa*0.001 log10(xvv)*0.3 ; log10(xhh)*0.3 log10(xdd)*0.3 ] ;
figure ; imshow(X11)

[r,c,s] = size(xv) ;
figure ; imshow([X11(1:r,1:c,:) xv*0.05 ; xh*0.05 xd*0.05 ])