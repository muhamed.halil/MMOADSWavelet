clear all;
close all;

% ucitavamo sliku i dodajemo noise
I = imread('SlikeZaCode/dodik.jpg');

% dodajemo noise - cisto da vidio efekat primjene transoformacije
A = imnoise(I,'salt & pepper',0.02);
% figure(1), imshow(I)

% pretvaramo sliku u crno-bijelu
Abw2=rgb2gray(A);

% za racun trebamo realne brojeve
Abw=double(Abw2);

% dimenzije slike (broj piksela)
[nx,ny]=size(Abw);

% crno-bijela verzija
figure(1), subplot(2,2,1), imshow(Abw2)

% 2-D wavelet transformacija (Haar)
[C,S]=wavedec2(Abw,2,'haar');

% normaliziramo os x
xw=(1:nx*ny)/(nx*ny);

% graficki prikaz wavelet koeficijenata originalne crno-bijele slike
figure(2), subplot(4,1,1), plot(xw,C,'k')

% tresholdi za rezanje koeficijenta - na 1000 komplet crno
th=[25 100 250];

% sazimanje po th(j)
for j=1:3
    %brojac koeficijenata
    count=0;

    % koeficijente wavelet transformacije spremamo
    % u pomocnu matricu prije svakog postupka rezanja
    C2=C;

	%za svaku od zadanih th(j) gledamo koef.
    for k=1:length(C2);

		%provjeravamo je li koeficijent
        if abs(C2(k)) < th(j)
        	% manji od dane donje granice (po apsolutnoj vrijednosti)
            % ako je koef. manji, postavi ga na 0
            C2(k)=0;

            %ukupni broj trivijalnih koef.
            count=count+1;
        end
    end

    %odredimo postotak trivijalnih koeficijenata
    percent=count/length(C2)*100

    %graficki prikaz wavelet koeficijenata i granice
    figure(2),
    subplot(4,1,j+1),plot(xw,C2,'k',[0 1],[th(j) th(j)],'r')

    % rekonstrukcija slike:
    Abw2_sparse=waverec2(C2,S,'haar');

    % vracamo koeficijente u raspon 0-255:
    Abw2_sparse2=uint8(Abw2_sparse);

    % racunamo MSE:
    D = abs(Abw-double(Abw2_sparse)).^2;
    MSE=sum(D(:)) / (nx*ny)

    % racunamo PSNR:
    PSNR=10* log10(255^2/MSE)

    %prikaz kompresovane slike u nizu
    figure(1), subplot(2,2,j+1), imshow(Abw2_sparse2)
end