% ---------------------------

[snd, sampFreq, nBits] = wavread('c1_01.wav');
size(snd)
sound(snd, 44100, 16)
s1 = snd(:,1);
timeArray = (0: 104158-1) / sampFreq;
timeArray = timeArray * 1000; %skalira u milisekunde
plot(timeArray, s1)
grid;

% ---------------------------

n = length(s1);
p = fft(s1); % proracunava fourierovu transformaciju
nUniquePts = ceil((n+1)/2);
p = p(1:nUniquePts);
% izabire prvu polovinu jer je druga polovina
% zrcalna slika prve
p = abs(p); % uzima apsolutnu vrijednost ili magnitudu
p = p/n; % skalira s brojem tocaka signala pa
% magnituda ne ovisi o duzini signala
% niti o frekvenciji sampliranja
p = p.^2; % kvadriramo da dobijemo snagu
% mnozi s dva
if rem(n, 2) % neparni nfft iskljucuje Nyquistovu tocku
p(2:end) = p(2:end)*2;
else
p(2:end -1) = p(2:end -1)*2;
end
freqArray = (0:nUniquePts-1) * (sampFreq / n);
% kreira frekvencijsku postavu
plot(freqArray/1000, 10*log10(p))
grid;
xlabel('Frekvencija (kHz)')
ylabel('Snaga (dB)')

% ---------------------------

waveinfo('sym')

% ---------------------------

[psi,xval] = wavefun('morl',10);
plot(xval,psi); title('Morlet Wavelet');

% ---------------------------

[phi,psi,xval] = wavefun('db4',10);
subplot(211);
plot(xval,phi);
title('db4 Scaling Function');
subplot(212);
plot(xval,psi); title('db4 Wavelet');

% ---------------------------

[psi,xval] = wavefun('mexh',10);
plot(xval,psi); title('Mexican Hat Wavelet');

% ---------------------------

[psi,xval] = wavefun('morl',10);
plot(xval,psi);
title('Real-valued Morlet Wavelet');

% ---------------------------

% Skup filtera Daubechies-5 obitelji valića
% Određujemo tip valića Daubechies 5
wname = 'db5';
% Računamo četiri pridružena filtera
određenom tipu valića prema stringu
wname.
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(wname);
subplot(221);
stem(Lo_D);
title('Dekompozicijski niskopropusni filter');
subplot(222);
stem(Hi_D);
title('Dekompozicijski visokopropusni filter');
subplot(223);
stem(Lo_R);
title('Rekonstrukcijski niskopropusni filter');
subplot(224);
stem(Hi_R);
title('Rekonstrukcijski visokopropusni filter');
xlabel(‘Obitelj četiri db5 filtera')

% ---------------------------

fvtool(Lo_D,1,Hi_D,1)
Dekompozicijski filter


% ---------------------------

fvtool(Lo_R,1,Hi_R,1)
Rekonstrukcijski filter

% ---------------------------

t = linspace(0,1,1000); x = 4*sin(4*pi*t);
x = x - sign(t - .3) - sign(.72 - t);
plot(t,x);
xlabel('t');
ylabel('x');
grid on;

% ---------------------------

W = ndwt(x,4,'db2','mode','per');
d1 = indwt(W,'d',1);

% ---------------------------

subplot(211);
plot(t,x);
title('Izvorni signal');
grid on;
subplot(212);
plot(t,d1,'r','linewidth',2);

% ---------------------------

title('1 razinska aproksimacija valićem');
grid on;

% ---------------------------

Fs = 1000;
t = 0:1/Fs:1;
Frq = 10;
x = sin(2*pi*t*Frq);
plot(x,'r'); axis tight
title('Signal');
xlabel('Time or Space')

% ---------------------------

h = spectrum.welch;
Hpsd = psd(h,x,'Fs',Fs);
hLIN = plot(Hpsd);
xdata = get(hLIN,'XData');
ydata = get(hLIN,'ydata');
[dummy,idxMax] = max(ydata);
FreqMax = xdata(idxMax)
hold on
ylim = get(gca,'YLim');
plot([FreqMax,FreqMax],ylim,'m--')
FreqMax = 7.8125

% ---------------------------

wname = 'gaus4';
scales = 1:1:128;
coefs = cwt(x,scales,wname);

% ---------------------------

TAB_Sca2Frq = scal2frq(scales,wname,1/Fs);
clf; plot(TAB_Sca2Frq); axis tight; grid
hold on
plot([scales(1),scales(end)],[Frq Frq],'m--')
set(gca,'YLim',[0 100])
title('Correspondence Table of Scales and Frequencies');
xlabel('Scale')
ylabel('Frequency')


% ---------------------------

[~,idxSca] = min(abs(TAB_Sca2Frq-Frq));
Sca = scales(idxSca)
Sca = 50
wscalogram('image',coefs,'scales',scales,'ydata',x);
hold on
plot([1 size(coefs,2)],[Sca Sca],'Color','m','LineWidth',2);

% ---------------------------

clf; coefs = cwt(x,scales,wname,'scalCNT');
hold on
plot([1 size(coefs,2)],[Sca Sca],'Color','m','LineWidth',2);

% ---------------------------

wname = 'mexh';
TAB_Sca2Frq = scal2frq(scales,wname,1/Fs);
[~,idxSca] = min(abs(TAB_Sca2Frq-Frq));
Sca = scales(idxSca)
Sca = 25
clf; coefs = cwt(x,scales,wname,'scalCNT');
hold on
plot([1 size(coefs,2)],[Sca Sca],'Color','m','LineWidth',2);

% ---------------------------

wname = 'morl';
TAB_Sca2Frq = scal2frq(scales,wname,1/Fs);
[~,idxSca] = min(abs(TAB_Sca2Frq-Frq));
Sca = scales(idxSca)
Sca = 81
clf; coefs = cwt(x,scales,wname,'scalCNT');
hold on
plot([1 size(coefs,2)],[Sca Sca],'Color','m','LineWidth',2);

% ---------------------------

wname = 'haar';
TAB_Sca2Frq = scal2frq(scales,wname,1/Fs);
[~,idxSca] = min(abs(TAB_Sca2Frq-Frq));
Sca = scales(idxSca)
Sca = 100
clf; coefs = cwt(x,scales,wname,'scalCNT');
hold on
plot([1 size(coefs,2)],[Sca Sca],'Color','m','LineWidth',2);

% ---------------------------

load dspwlets; % load wavelet coefficients and noisy signal
Threshold = [3 2 1 0];
%Create a SignalReader System object to output the noisy signal.
hsfw = dsp.SignalReader(noisdopp.', 64);
%Create and configure a DyadicAnalysisFilterBank System object for wavelet
decomposition of the signal.
hdyadanalysis = dsp.DyadicAnalysisFilterBank( ...
'CustomLowpassFilter', lod, ...
'CustomHighpassFilter', hid, ...
'NumLevels', 3);
%Create three Delay System objects to compensate for the system delay
introduced by the wavelet components.
hdelay1 = dsp.Delay(3*(length(lod)-1));
hdelay2 = dsp.Delay(length(lod)-1);
hdelay3 = dsp.Delay(7*(length(lod)-1));
%Create and configure a DyadicSynthesisFilterBank System object for wavelet
reconstruction of the signal.
hdyadsynthesis = dsp.DyadicSynthesisFilterBank( ...
'CustomLowpassFilter', lor, ...
'CustomHighpassFilter', hir, ...
'NumLevels', 3);
%Create time scope System object to plot the original, denoised and residual
signals.
hts = dsp.TimeScope('Name', 'Wavelet Denoising', ...
'SampleRate', fs, ...
'TimeSpan', 13, ...
'NumInputPorts', 3, ...
'LayoutDimensions',[3 1], ...
'TimeAxisLabels', 'Bottom');
pos = hts.Position;
hts.Position = [pos(1) pos(2)-(0.5*pos(4)) 0.9*pos(3) 2*pos(4)];
% Set properties for each display
hts.ActiveDisplay = 1;
hts.Title = 'Input Signal';
hts.ActiveDisplay = 2;
hts.Title = 'Denoised Signal';
hts.ActiveDisplay = 3;
hts.Title = 'Residual Signal';
%Stream Processing Loop
%Create a processing loop to denoise the input signal. This loop uses the System
objects you instantiated above.
for ii = 1:length(noisdopp)/64
sig = step(hsfw);
% Input noisy signal
S = step(hdyadanalysis, sig); % Dyadic analysis
% separate into four subbands
S1 = S(1:32); S2 = S(33:48); S3 = S(49:56); S4 = S(57:64);
% Delay to compensate for the dyadic analysis filters
S1 = step(hdelay1, S1);
S2 = step(hdelay2, S2);
S1 = dspDeadZone(S1, Threshold(1));
S2 = dspDeadZone(S2, Threshold(2));
S3 = dspDeadZone(S3, Threshold(3));
S4 = dspDeadZone(S4, Threshold(4));
% Dyadic synthesis (on concatenated subbands)
S = step(hdyadsynthesis, [S1; S2; S3; S4]);
sig_delay = step(hdelay3, sig);
% Delay to compensate for analysis/synthesis.
Error = sig_delay - S;
% Plot the results
step(hts, sig_delay, S, Error);
end

% ---------------------------

F1 = 10;
F2 = 40;
Fs = 1000;
t = 0:1/Fs:1;
x = sin(2*pi*t*F1) + sin(2*pi*t*F2);
wn = randn(1,length(x));
wn = 1.5*wn/std(wn);
xn = x + wn;
plot (xn)

% ---------------------------

h = spectrum.welch;
Hpsd = psd(h,xn,'Fs',Fs);
clf; hLIN = plot(Hpsd);
ydata_XN = get(hLIN,'ydata');

% ---------------------------

wname = 'gaus4';
scales = 1:1:128;
TAB_Sca2Frq = scal2frq(scales,wname,1/Fs);
[~,idxSca_1] = min(abs(TAB_Sca2Frq-F1));
Sca_1 = scales(idxSca_1)
[mini,idxSca_2] = min(abs(TAB_Sca2Frq-F2));
Sca_2 = scales(idxSca_2)

Sca_1 = 50
Sca_2 = 13

coefs = cwt(xn,scales,wname);
clf; wscalogram('image',coefs,'scales',scales,'ydata',xn);
hold on
plot([1 size(coefs,2)],[Sca_1 Sca_1],'Color','m','LineWidth',2);
plot([1 size(coefs,2)],[Sca_2 Sca_2],'Color','w','LineWidth',1);


% ---------------------------

F1 = 10;
F2 = 40;
Fs = 1000;
t = 0:1/Fs:1;
x = sin(2*pi*t*F1).*((t<0.25)+(t>0.75)) + sin(2*pi*t*F2).*(t>0.25).*(t<0.75);
plot (x)
%prikaz signala
clf;
plot(x);axis tight
title('Signal');
xlabel('vrijeme ili prostor')

% ---------------------------

h = spectrum.welch;
Hpsd = psd(h,x,'Fs',Fs);
clf; plot(Hpsd);

% ---------------------------

wname = 'gaus4';
scales = 1:1:128;
coefs = cwt(x,scales,wname);
clf; wscalogram('image',coefs,'scales',scales,'ydata',x);
hold on
plot([1 size(coefs,2)],[Sca_1 Sca_1],'Color','m');
plot([1 size(coefs,2)],[Sca_2 Sca_2],'Color','m');
plot([250 250],[1 128],'Color','g','LineWidth',2);
plot([750 750],[1 128],'Color','g','LineWidth',2);


% ---------------------------

x=analog(100,4,40,10000);
% sinusoida frekvencije 100 Hz
amplitude 4
xn=x+0.5*randn(size(x));
% dodajemo Gaussov šum
[cA,cD]=dwt(xn,'db8');
% računamo prvu razinu dekompozicije
% s dwt i valićem Daubechies 8
subplot(3,1,1),
plot(xn),
title('Izvorni signal')
subplot(3,1,2),
plot(cA),
title('Jednorazinska aproksimacija')
subplot(3,1,3),
plot(cD),
title('Jednorazinski detalj')

% ---------------------------

fs=2500;
len=100;
[x1,t1]=analog(50,.5,len,fs);
% vremenski vektor t1 je u ms
[x2,t2]=analog(100,.25,len,fs);
[x3,t3]=analog(200,1,len,fs);
y1=cat(2,x1,x2,x3);
% ulančavamo signale
ty1=[t1,t2+len,t3+2*len];
% ulančavamo vektore vremena
% 1 u len, len u 2*len…
[A1,D1]=dwt(y1,'db8');
subplot(3,1,1),
plot(y1),
title('Izvorni signal')
subplot(3,1,2),
plot(A1),
title('Jednorazinska aproksimacija')
subplot(3,1,3),
plot(D1, 'r'),
title('Jednorazinski detalj')

% ---------------------------

x=analog(100,4,40,10000);
xn=x+0.5*randn(size(x));
[C,L] = wavedec(xn,4,'db8');
% Višerazinska analiza do 4 razine sa valićem Daubechies 8
A1 = wrcoef('a',C,L,'db8',1);
% Rekonstruiraj aproksimacije na 4 razine
A2 = wrcoef('a',C,L,'db8',2);
A3 = wrcoef('a',C,L,'db8',3);
A4 = wrcoef('a',C,L,'db8',4);
subplot(5,1,1),
plot(xn,'r'),
title('Izvorni signal')
subplot(5,1,2),
plot(A1),
title('Rekonstrirana aproksimacija prve razine')
subplot(5,1,3),
plot(A2),
title('Rekonstrirana aproksimacija druge razine')
subplot(5,1,4),
plot(A3),
title('Rekonstrirana aproksimacija trede razine')
subplot(5,1,5),
plot(A4),
title('Rekonstrirana aproksimacija četvrte razine')

% ---------------------------

x=analog(100,4,40,10000);
x(302:303)=.1;
[A,D]=dwt(x,'db8');
subplot(3,1,1),
plot(x),
title('Izvorni signal')
subplot(3,1,2),
plot(A),
title('Aproksimacija prve razine')
subplot(3,1,3),
plot(D,'r'),
title('Detalj prve razine')

% ---------------------------

load leleccum % datoteka u MATLAB-u
x=leleccum;
w = 'db3';
[C,L] = wavedec(x,4,w);
A4 = wrcoef('a',C,L,'db3',4);
A3 = wrcoef('a',C,L,'db3',3);
A2 = wrcoef('a',C,L,'db3',2);
A1 = wrcoef('a',C,L,'db3',1);
a3 = appcoef(C,L,w,3);
subplot(2,1,1),
plot(x),axis([0,4000,100,600])
title('Izvorni signal')
subplot(2,1,2),
plot(A3,'r'),axis([0,4000,100,600])
title('Aproksimacijska rekonstrukcija 3 razine
primjenom valića Daubechies 3 ')
(length(a3)/length(x))*100

% ---------------------------

load leleccum
x=leleccum;
w = 'db3'; % Određujemo valić Daubechies-4
[C,L] = wavedec(x,4,w);
% Višerazinska dekompozicija u 4 razine
a3 = appcoef(C,L,w,3);
% Ekstrakcija aproksimacijskih koeficijenata 3
razine
d3 = detcoef(C,L,3);
% Ekstrakcija koeficijenata detalja 3 razine
subplot(2,1,1),
plot(a3),grid
title('Aproksimacijski koeficijenti 3 razine')
subplot(2,1,2),
plot(d3, 'r'), grid
title('Koeficijenti detalja 3 razine')

% ---------------------------

load leleccum
% učitavamo zvučnu datoteku iz Matlaba
x=leleccum; % Nesažeti signal x
w = 'db3';
% Određujemo obitelj valića w
n=3;
% Određujemo razinu dekompozicije n
[C,L] = wavedec(x,n,w);
% Određujemo strukturu dekompozicije x za razinu n
primjenom w.
thr = 10; % Određujemo vrijednost praga
keepapp = 1;
%Logički parameter = koeficijentima aproksimacije ne
traži prag
sorh='h'; % Primjeni tvrdi prag
[xd,cxd,lxd, perf0,perfl2]
=wdencmp('gbl',C,L,w,n,thr,sorh,keepapp);
subplot(2,1,1), plot(x),title('Izvorni signal')
grid
subplot(2,1,2),plot(xd,'r'),title('Sažeti signal (Detalj
praga)')
grid
perf0 % Postotak koeficijenata postavljen na nulu
perfl2 % Postotak zadržane energije u sažetom signalu
perf0 =
83.4064
perfl2 =
99.9943

% ---------------------------

D1 = wrcoef('d',C,L,w,1);
D2 = wrcoef('d',C,L,w,2);
D3 = wrcoef('d',C,L,w,3);
d1 = wrcoef('d',cxd,lxd,w,1);
d2 = wrcoef('d',cxd,lxd,w,2);
d3 = wrcoef('d',cxd,lxd,w,3);
subplot(3,2,1),plot(D3),
title('Izvorni detalji od razine 3
do 1')
subplot(3,2,2),plot(d3),
title('Detalji praga od razine 3 do
1')
subplot(3,2,3),plot(D2,'k')
subplot(3,2,4),plot(d2,'k')
subplot(3,2,5),plot(D1,'r')
subplot(3,2,6),plot(d1,'r')

% ---------------------------



% ---------------------------



% ---------------------------



% ---------------------------



% ---------------------------