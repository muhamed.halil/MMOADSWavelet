\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Pregled}{3}{0}{1}
\beamer@sectionintoc {2}{Uvod i pozadina uvo\IeC {d\kern -0.4em\char "16\kern -0.1em}enja Wavelet transformacija}{5}{0}{2}
\beamer@sectionintoc {3}{Wavelet transformacija}{11}{0}{3}
\beamer@sectionintoc {4}{Kori\IeC {\v s}tenje Matlabovog Wavelet Toolbox-a}{21}{0}{4}
\beamer@sectionintoc {5}{Primjene wavelet transformacije}{23}{0}{5}
\beamer@subsectionintoc {5}{1}{Kompresija slike}{24}{0}{5}
\beamer@subsectionintoc {5}{2}{Uklanjanje \IeC {\v s}uma iz signala}{36}{0}{5}
\beamer@subsectionintoc {5}{3}{Prepoznavanje osobe na osnovu ociju}{43}{0}{5}
\beamer@sectionintoc {6}{Zaklju\IeC {\v c}ci i Reference}{48}{0}{6}
